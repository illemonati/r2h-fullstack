
mod build_db;
mod read_outputs;
mod run_server;

use clap::Clap;
use stopwatch::Stopwatch;

#[derive(Clap)]
#[clap(version = "1.0", author = "Tong M. <tonymiaotong@tioft.tech>")]
struct Opts {
    /// Sets a custom config file. Could have been an Option<T> with no default too
    #[clap(short, long, default_value = "./db_stuff")]
    dbpath: String,
    #[clap(subcommand)]
    subcmd: SubCommand,
}

#[derive(Clap)]
enum SubCommand {
    Build(Build),
    Run(Run),
}


#[derive(Clap)]
struct Build {
    #[clap(short, long, default_value = "./assets/outputs.json")]
    source_json_path: String,
}

#[derive(Clap)]
struct Run {
    #[clap(short, long, default_value = "0.0.0.0")]
    host: String,
    #[clap(short, long, default_value = "8080")]
    port: String,
}


#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opts: Opts = Opts::parse();

    println!("using {} as db path", &opts.dbpath);

    match opts.subcmd {
        SubCommand::Build(b) => {
            println!("using {} as src json path", &b.source_json_path);
            println!("starting build");
            let sw = Stopwatch::start_new();
            let diseases = read_outputs::get_diseases_from_outputs_file(&b.source_json_path)?;
            let _ = build_db::create_db(&opts.dbpath, &diseases); 
            println!("done in {} secs", sw.elapsed().as_secs_f32());
        },
        SubCommand::Run(r) => {
            let local = tokio::task::LocalSet::new();
            let sys = actix_rt::System::run_in_tokio("server", &local);
            let host_port_str = &format!("{}:{}", r.host, r.port);
            println!("Starting server on {}", host_port_str);
            run_server::run_server(host_port_str).await?;
            sys.await?;
        }
    }

    Ok(())
}
