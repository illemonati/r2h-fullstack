use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs;

#[derive(Serialize, Deserialize, Debug)]
pub struct StandardSections {
    pub about: Option<HashMap<String, String>>,
    pub symptoms: Option<HashMap<String, String>>,
    #[serde(alias = "peopleAtRisk")]
    pub people_at_risk: Option<HashMap<String, String>>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Disease {
    pub name: String,
    pub link: String,
    pub description: String,
    pub sections: HashMap<String, String>,
    #[serde(alias = "standardSections")]
    pub standard_sections: StandardSections,
}

pub fn get_diseases_from_outputs_file(
    filepath: &str,
) -> Result<Vec<Disease>, Box<dyn std::error::Error + 'static>> {
    let outputs_string = fs::read_to_string(filepath)?;
    // println!("{}", outputs_string);
    let dieseases: Vec<Disease> = serde_json::from_str(&outputs_string)?;
    // println!("{:?}", dieseases);
    Ok(dieseases)
}
