#! /usr/bin/env bash

git submodule update --init --recursive --remote
cd ./static/
yarn
yarn build
cd ..
cargo build --release
./target/release/r2h run -p 32412
